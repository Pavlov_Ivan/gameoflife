#include <cstdlib>
#include <ctime> 
#include <cstdio>
#include <string.h>
#include <mpi.h>

#include <vtkVersion.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>

const bool LIVE = true;
const bool DEAD = false;

class GameOfLife 
{
public:
void init(int size, int argc, char ** argv);
int countLiveAround(int i, int j);
void nextStep();
void print();
void MPISynchronize();
int getRank(){return mpi_rank;}
int getStepNum(){return stepNum;}
~GameOfLife();
private:
bool **curUniverse;
bool **newUniverse;
int row;
int col;
int stepNum;
int mpi_rank;
int mpi_size;
};

/* Инициализируем MPI, выделяем память, под чать вселенной, 
 * которая будет обрабатываться нашим процессом, 
 * размещаем клетки случайным образом 
 * 4 - это magic number, который я не придумал как назвать*/
void GameOfLife::init(int size, int argc, char ** argv)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    srand(time(0) + mpi_rank);
    row = size/mpi_size;
    assert(size%mpi_size == 0);
    col = size;
    stepNum = 0;
    curUniverse = new bool *[row + 2];
    newUniverse = new bool *[row + 2];
    for(int i = 0; i <= row + 1; ++i){
        curUniverse[i] = new bool[col + 2];
        newUniverse[i] = new bool[col + 2];
        for(int j = 0; j<= col + 1; ++j){
            curUniverse[i][j] = DEAD;
            newUniverse[i][j] = DEAD;
        }
    }
    for(int i = 1; i <= row; ++i){
        for(int j = 1; j <= col; ++j){
            if(rand() % 4)
                curUniverse[i][j] = DEAD;
            else
                curUniverse[i][j] = LIVE;
        }
    }
}

/* Считаем количество живых клеток вокруг точки с координатами i, j*/
int GameOfLife::countLiveAround(int i, int j)
{
    int count = 0;
    for(int k = i-1; k <= i+1; k++)
        for(int l = j-1; l <= j+1; l++)
            if(!(k == i && l == j) && curUniverse[k][l] == LIVE)
                ++count;
    return count;
}

void GameOfLife::nextStep()
{
    for(int i = 1; i <= row; ++i){
        for(int j = 1; j <= col; ++j){
            int cn = countLiveAround(i, j);
            if(curUniverse[i][j] == DEAD && cn == 3)
                newUniverse[i][j] = LIVE;
            else if(cn < 2 || cn > 3)
                newUniverse[i][j] = DEAD;
            else
                newUniverse[i][j] = curUniverse[i][j];
        }
    }
    bool **tmp = curUniverse;
    curUniverse = newUniverse;
    newUniverse = tmp;
    ++stepNum;
}

/* Вывод результатов в vtu файл
 * j - координата по X
 * row * mpi_rank + i - координата по Y*/
void GameOfLife::print()
{
    char fname[32];
    sprintf(fname, "output_%d_%d.vtu", stepNum, mpi_rank);
    vtkSmartPointer<vtkPoints> points =
        vtkSmartPointer<vtkPoints>::New();
    for(int i = 1; i <= row; ++i){
        for(int j = 1; j <= col; ++j){
            if(curUniverse[i][j] == LIVE)
            {
//                putchar('x');
                points->InsertNextPoint(j, row * mpi_rank + i, 0);
            }
            else
            {
//                putchar('-');
            }
        }
//        printf(" rank = %d step = %d", mpi_rank, stepNum);
//        putchar('\n');
    }
    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid =
        vtkSmartPointer<vtkUnstructuredGrid>::New();
    unstructuredGrid->SetPoints(points);
 
    // Write file
    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer =
        vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
    writer->SetFileName(fname);
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(unstructuredGrid);
#else
    writer->SetInputData(unstructuredGrid);
#endif
    writer->Write();
}

/* Функция "сшиает" левую и правую части кусочка вселенной
 * и "сшивает" этот кусочек вселенной с тем который выше и ниже
 * строка массива передается просто как последовательность байт
 * тут есть потенциальные грабли с big/little endian 
 * и разным размером bool на разных архитектурах*/
void GameOfLife::MPISynchronize()
{
    MPI_Status st;
    int bottom_rank = (mpi_rank - 1 + mpi_size) % mpi_size;
    int top_rank = (mpi_rank + 1) % mpi_size;
    for (int i = 1; i <= row; i++){
        curUniverse[i][0]     = curUniverse[i][col];
        curUniverse[i][col+1] = curUniverse[i][1];
    }
    MPI_Sendrecv(curUniverse[1], (col+2)*sizeof(bool), MPI_CHAR, top_rank, 0,
                curUniverse[row+1], (col+2)*sizeof(bool), MPI_CHAR, bottom_rank, 0,
                MPI_COMM_WORLD, &st);

    MPI_Sendrecv(curUniverse[row], (col+2)*sizeof(bool), MPI_CHAR, bottom_rank, 0,
                curUniverse[0], (col+2)*sizeof(bool), MPI_CHAR, top_rank, 0,
                MPI_COMM_WORLD, &st);
}

/* Очищаем выделенные ресурсы*/
GameOfLife::~GameOfLife()
{
    for(int i = 0; i <= row + 1; ++i)
    {
        delete[] curUniverse[i];
        delete[] newUniverse[i];

    }
    delete[] curUniverse;
    delete[] newUniverse;
    MPI_Finalize();
}

int main(int argc, char ** argv)
{
    GameOfLife game;
    game.init(10, argc, argv);
    char ch = '\0';
    while(true){
        game.print();
        if(game.getRank() == 0){
            printf("Step №%d\n", game.getStepNum());
            printf("Press enter to next, press q + enter to quit\n");
            ch = getchar();
            if(ch == 'q')
                break;
        }
        game.MPISynchronize();
        game.nextStep();
    }
    MPI_Abort(MPI_COMM_WORLD, 0);
    return 0;
}
