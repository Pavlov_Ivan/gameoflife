cmake_minimum_required(VERSION 2.8)
 
PROJECT(GameOfLife)

set(CMAKE_CXX_COMPILER mpic++) 
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})
 
add_executable(GameOfLife MACOSX_BUNDLE GameOfLife)
 
if(VTK_LIBRARIES)
  target_link_libraries(GameOfLife ${VTK_LIBRARIES})
else()
  target_link_libraries(GameOfLife vtkHybrid vtkWidgets)
endif()
